const keys = require('../keys');


module.exports = function(email, token){
    return { 
        to : email,
        from : keys.EMAIL_FROM,
        subject : 'Change password',
        html : `<a href="${keys.BASE_URL}/auth/password/${token}">Follow link to change password</a>`
    }

}