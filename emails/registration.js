const keys = require('../keys');


module.exports = function(email){
    return { 
        to : email,
        from : keys.EMAIL_FROM,
        subject : 'Account has been created',
        html : `<a href="${keys.BASE_URL}">Back to shop</a>`
    }

}