M.Tabs.init(document.querySelectorAll('.tabs'));

const toCurrency = price =>{
    return  new Intl.NumberFormat("ru-RU", {
        currency: "rub",
        style: "currency"
    }).format(price);
}

document.querySelectorAll('.price').forEach(element => {
        element.textContent = toCurrency(element.textContent)        
    });

const toDateFormat = date =>{
    return new Intl.DateTimeFormat("en-US", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "2-digit",
        hour : '2-digit',
        minute : '2-digit',
        second : "2-digit"
      }).format(new Date(date));
}
document.querySelectorAll('.date').forEach(element => {
    element.textContent = toDateFormat(element.textContent)        
});

const $card = document.querySelector('#card');

$card.addEventListener('click', event=>{
    if(event.target.classList.contains('js-remove')){
        const id = event.target.dataset.id;
        const csrf = event.target.dataset.csrf;
        console.log(csrf);
        fetch('/card/remove/' + id, {
            method : 'delete',
            headers : {
                'X-XSRF-TOKEN' : csrf
            },
        }).then(res => res.json())
        .then(card=> {
            if(card.courses.length){ 
           const html = card.courses.map(el => {
                return `
                <tr>
                <td>${el.title}</td>
                <td>${el.count}</td>
            <td>
                <button class="btn btn-small js-remove" data-id="${el.id}">Delete</button>
            </td>
            </tr>
                `
            }).join('');
            $card.querySelector('tbody').innerHTML = html;
            $card.querySelector('.price').textContent = toCurrency(card.price);
        }else{
            $card.innerHTML = `<p>Any courses added yet</p>`
        }

            
        });
    }
});



