const {Router} = require('express');
const Course = require('../models/course')
const router = Router();
const {validationResult} = require('express-validator');
const auth = require('../middleware/auth');
const User = require('../models/user');
const {courseValidators} = require('../utils/validators');

router.get('/', auth, (req, res)=>{
    res.render('add.hbs', {
        title : 'Add Page',
        isAdd : true
    })
});

router.post('/', auth, courseValidators, async (req,res)=>{

    const errors = validationResult(req);

    if(!errors.isEmpty()){
        return res.status(422).render('add', {
            title : 'Add Page',
            isAdd : true,
            errors : errors.array()[0].msg,
            data : {
                title : req.body.title,
                price : req.body.price,
                img : req.body.img
            }
        })
    }


    const course = new Course({
        title : req.body.title,
        price : req.body.price,
        img : req.body.img,
        userId : req.user
    });
   
    try{
        await course.save(); 
        res.redirect('/courses')
    }catch(e){
        console.log(e);
    }
});

router.post('/password', async(req,res)=>{

    const user = await User.findOne({
        _id : req.body.userId,
        resetToken
    })
})

module.exports = router;