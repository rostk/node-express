const {Router} = require('express');
const router = Router();
const Course = require('../models/course')
const auth = require('../middleware/auth');
const {validationResult} = require('express-validator')
const {courseValidators} = require('../utils/validators');


router.get('/', async (req, res)=>{
    const courses = await Course.find()
        .populate('userId', 'email name')
        .select('price title img');
        
    res.render('courses', {
        title : 'Courses Page',
        isCourses : true,
        userId : req.user ? req.user._id.toString() : null,
        courses
        
    })
});

router.get('/:id/edit', auth, async (req,res)=>{
    if(!req.query.allow){
        return res.redirect('/');
    }
    try{
        const course = await Course.findById(req.params.id);
            if(course.userId.toString() === req.user._id.toString()){
            res.render('course-edit', {
                title : `Edit Course ${course.title}`,
                course
            });
            } else {
                return res.redirect('/courses');
            }
    }catch(e){
        console.log(e);
    }
    
})

router.post('/edit', auth, courseValidators, async (req,res)=>{
    const {id} = req.body;
    const errors = validationResult(req);

    if(!errors.isEmpty()){
        return res.status(422).redirect(`/courses/${id}/edit?allow=true`);
    }
    
    delete req.body.id;
    try{
        if(course.userId.toString() === req.user._id.toString()){
            await Course.findByIdAndUpdate(id, req.body);
            res.redirect('/courses');
        } else {
                res.redirect('/courses');
            }
    
    } catch(e){
        console.log(e);
    }
       
   
})



router.get('/:id', async (req,res)=>{
    try{
        const course = await Course.findById(req.params.id)
            res.render('course', {
                title : `Course ${course.title}`,
                course
            })
    } catch(e){
        console.log(e);
    }
    
})

router.post('/remove', auth, async(req,res)=>{
    try{
        await Course.deleteOne({
            _id : req.body.id,
            userId : req.user._id
        })
        res.redirect('/courses');
    }catch(e){
        console.log(e)
    }
    
})




module.exports = router;