const {body} = require('express-validator');
const User = require('../models/user');


exports.registerValidators = [
    body('email', 'Email is not correct,please try again').isEmail().custom(async(value, {req})=>{
        try{
            const user = await User.findOne({email : value});
            if(user){
                return Promise.reject(`Email ${value} already exist,please try again`)
            }
        }catch(e){
            console.log(e);
        }
    }),
    body('name', 'Name has to be more than 3 symbols').isLength({min : 3}),
    body('password', 'Password has to be minimum 6 symbols').isLength({min : 6, max : 56}).isAlphanumeric(),
    body('confirm').custom((value, {req})=>{
        if(value !== req.body.password){
            throw new Error('passwords has to match');
        }
        return true;
    })
];

// exports.loginValidator = [
//     do it for real project
// ]

exports.courseValidators = [
    body('title', 'Your title course has to be more than 3 symbols').isLength({min : 3}),
    body('price', 'Price is incorrect').isNumeric(),
    body('img').isURL()
];