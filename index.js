const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const app = express();
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const helmet = require('helmet');
const compression = require('compression');
const homeRouter = require('./routs/home');
const coursesRouter = require('./routs/courses');
const addRouter = require('./routs/add');
const cardRouter = require('./routs/card');
const ordersRouter = require('./routs/orders');
const authRouter = require('./routs/auth');
const profileRouter = require('./routs/profile');
const keys = require('./keys')
const varMiddleware = require('./middleware/variables');
const userMiddleware = require('./middleware/user');
const fileMiddleware = require('./middleware/file');
const errorHandler = require('./middleware/error');

const hbs = exphbs.create({
    defaultLayout : 'main',
    extname : 'hbs',
    helpers : require('./utils/hbs-helper.js')
})

app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');

const store = new MongoStore({
    collection : 'sessions',
    uri : keys.MONGODB_URI
});


app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use(express.urlencoded({extended : true})); //buffer parse (post req)

app.use(session({
    secret : keys.SESSION_SECRET,
    resave : false,
    saveUninitialized : false,
    store
}));
app.use(fileMiddleware.single('avatar'));
app.use(csrf());
app.use(flash());
app.use(helmet());
app.use(compression());

app.use(varMiddleware);
app.use(userMiddleware);


app.use('/', homeRouter);
app.use('/courses', coursesRouter);
app.use('/add', addRouter);
app.use('/card', cardRouter);
app.use('/orders', ordersRouter);
app.use('/auth', authRouter);
app.use('/profile', profileRouter);

app.use(errorHandler);


const PORT = process.env.PORT || 3000;
async function start(){
    try{
        await mongoose.connect(keys.MONGODB_URI, {useNewUrlParser : true, useUnifiedTopology: true, useFindAndModify: false});
        app.listen(PORT, ()=> console.log(`server has been started on port ${PORT}`));

    }catch(e){
        console.log(e);
    }
    
}

start();




